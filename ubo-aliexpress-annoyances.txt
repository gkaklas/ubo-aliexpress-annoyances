! Title: gkaklas AliExpress Annoyances
! Description: Removes unnecessary elements from aliexpress
! Expires: 10 days
! Homepage: https://gitlab.com/gkaklas/ubo-aliexpress-annoyances
! License: https://gitlab.com/gkaklas/ubo-aliexpress-annoyances/blob/master/LICENSE
! Product page
www.aliexpress.com###j-related-products
www.aliexpress.com###j-transaction-feedback
www.aliexpress.com##.p-del-price-detail
! Left sidebar
www.aliexpress.com##.mobile-app.me-ui-box
www.aliexpress.com###historyOuterBox
! Floating
www.aliexpress.com##.ui-fixed-panel
www.aliexpress.com##.show-history
! List of products
www.aliexpress.com###survey-container
www.aliexpress.com###p4pHotProducts-old
www.aliexpress.com##.original-price
www.aliexpress.com##.new-discount-rate
www.aliexpress.com##.bp-horizontal-banner
